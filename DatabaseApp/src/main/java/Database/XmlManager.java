/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.io.File;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 *
 * @author User
 */
public class XmlManager {
    private DocumentBuilderFactory dbf;
    private DocumentBuilder builder;
    private Document doc;
    private Element rootElem;
    
    public boolean createXmlDocument(){
        
        try{
            dbf = DocumentBuilderFactory.newInstance();
            builder = dbf.newDocumentBuilder();
            doc = builder.newDocument();
            rootElem = doc.createElement("database_information");
            doc.appendChild(rootElem);
        }
        catch(Exception ex){   
            return false;
        }  
        return true;
    }
    
    public void addTableInfo(String tableName, List<ColumnInfo> columnInfo){
        Element tableElem = doc.createElement("table");
        
        Element tName = doc.createElement("name");
        tName.setTextContent(tableName);
        tableElem.appendChild(tName);
        
        Element columns = doc.createElement("columns");
        
        int columnInfoLen = columnInfo.size();
        for (int j = 0; j < columnInfoLen; j++){
            Element columnElem = doc.createElement("column");
            
            Element columnName = doc.createElement("name");
            columnName.setTextContent(columnInfo.get(j).getName());
            columnElem.appendChild(columnName);
            
            Element columnType = doc.createElement("type");
            columnType.setTextContent(columnInfo.get(j).getType());
            columnElem.appendChild(columnType);
           
            if (columnInfo.get(j).getMinValue() != null){
                Element minVal = doc.createElement("minValue");
                minVal.setTextContent(columnInfo.get(j).getMinValue().toString());
                columnElem.appendChild(minVal);
            }
            if (columnInfo.get(j).getMaxValue() != null){
                Element maxVal = doc.createElement("maxValue");
                maxVal.setTextContent(columnInfo.get(j).getMaxValue().toString());
                columnElem.appendChild(maxVal);
            }
            
            if (columnInfo.get(j).getMiddleValue() != null){
                Element middleVal = doc.createElement("middleValue");
                middleVal.setTextContent(columnInfo.get(j).getMiddleValue().toString());
                columnElem.appendChild(middleVal);
            }
            
            if (columnInfo.get(j).getDispersion() != null){
                Element dispersion = doc.createElement("dispersion");
                dispersion.setTextContent(columnInfo.get(j).getDispersion().toString());
                columnElem.appendChild(dispersion);
            }
            
            Element amountOfExportedKeys = doc.createElement("amountOfExportedKeys");
            int am = columnInfo.get(j).getAmountOfExportedKeys();
            amountOfExportedKeys.setTextContent(Integer.toString(columnInfo.get(j).getAmountOfExportedKeys()));
            columnElem.appendChild(amountOfExportedKeys);            
           
            if (columnInfo.get(j).getBarChart() != null){
                Element barChart = doc.createElement("barChart");
                int bcLen = columnInfo.get(j).getBarChart().size();
                for(int i = 0; i < bcLen; i++){
                    Element elem = doc.createElement("element");
                    
                    Element elemLeftBorder = doc.createElement("leftBorder");
                    elemLeftBorder.setTextContent(Double.toString(columnInfo.get(j).getBarChart().get(i).getLeftBorder()));
                    elem.appendChild(elemLeftBorder);
                    
                    Element elemRightBorder = doc.createElement("rightBorder");
                    elemRightBorder.setTextContent(Double.toString(columnInfo.get(j).getBarChart().get(i).getRightBorder()));
                    elem.appendChild(elemRightBorder);
                    
                    Element elemAmount = doc.createElement("amount");
                    elemAmount.setTextContent(Integer.toString(columnInfo.get(j).getBarChart().get(i).getAmount()));
                    elem.appendChild(elemAmount);
                   
                    barChart.appendChild(elem);
                }
                columnElem.appendChild(barChart);
            }
            columns.appendChild(columnElem);
        }
        tableElem.appendChild(columns);
        rootElem.appendChild(tableElem);
    }
    
    public void addRelationshipInfo(ForeignKeyInfo fkInfo){
        Element relationshipElem = doc.createElement("relationship");
        
        Element pkTableElem = doc.createElement("pkTable");
        pkTableElem.setTextContent(fkInfo.getPkTableName());
        relationshipElem.appendChild(pkTableElem);
        
        Element pkColumnElem = doc.createElement("pkColumn");
        pkColumnElem.setTextContent(fkInfo.getPkColumnName());
        relationshipElem.appendChild(pkColumnElem);
        
        Element fkTableElem = doc.createElement("fkTable");
        fkTableElem.setTextContent(fkInfo.getFkTableName());
        relationshipElem.appendChild(fkTableElem);
        
        Element fkColumnElem = doc.createElement("fkColumn");
        fkColumnElem.setTextContent(fkInfo.getFkColumnName());
        relationshipElem.appendChild(fkColumnElem);
        
        Element maxElem = doc.createElement("max");
        maxElem.setTextContent(Integer.toString(fkInfo.getMax()));
        relationshipElem.appendChild(maxElem);
        
        Element minElem = doc.createElement("min");
        minElem.setTextContent(Integer.toString(fkInfo.getMin()));
        relationshipElem.appendChild(minElem);
        
        Element middleValueElem = doc.createElement("middleValue");
        middleValueElem.setTextContent(Double.toString(fkInfo.getMiddleValue()));
        relationshipElem.appendChild(middleValueElem);
        
        Element dispersionElem = doc.createElement("dispersionValue");
        dispersionElem.setTextContent(Double.toString(fkInfo.getDispersion()));
        relationshipElem.appendChild(dispersionElem);
        
        rootElem.appendChild(relationshipElem);
    }
    
    public void saveToFile(String fileName){
        TransformerFactory tf = TransformerFactory.newInstance();
        try{
            removeWhitespaceNodes(doc.getDocumentElement());
            Transformer tr = tf.newTransformer();
            tf.setAttribute("indent-number", 2);
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.STANDALONE, "yes");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(doc);
            StreamResult sr = new StreamResult(new File(fileName));
            tr.transform(source, sr);
        }
        catch(Exception ex){
            System.out.println(ex.toString());
        }
    }
    
    private void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
}
