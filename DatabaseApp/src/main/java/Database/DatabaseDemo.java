/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class DatabaseDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*DatabaseManager dbm = new DatabaseManager();
        if(!dbm.connect("postgres", "2706537", "internetShop")){
            System.out.println("Error when connecting to the database");
            return;
        }
        */
        
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите имя пользователя: ");
        String user = scan.next();
        System.out.println("Введите пароль: ");
        String password = scan.next();
        System.out.println("Введите имя базы данных: ");
        String dbName = scan.next();
        
        
        StatisticManager sm = new StatisticManager(user, password, dbName);
        
        List<String> tableNames = sm.getTableNames();
        int tableNamesLen = tableNames.size();
       /* System.out.println("Tables' names:");
        for(int i = 0; i < tableNamesLen; i++){
            System.out.println(tableNames.get(i));
        }*/
        
        System.out.println("Введите максимальное количество интервалов для построения гистограммы: ");
        int maxAmountOfIntervals = 0;
        try{
            maxAmountOfIntervals = scan.nextInt();
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            return;
        }
        
        List<List<ColumnInfo>> colInfo = new ArrayList<>();
        for (int i = 0; i < tableNamesLen; i++){
            List<ColumnInfo> ls = sm.getColumnInfo(tableNames.get(i), maxAmountOfIntervals);
            colInfo.add(ls);
        }
        
        List<List<ForeignKeyInfo>> fkInfoList = new ArrayList<>();
        for (int i = 0; i < tableNamesLen; i++){
            String tableName = tableNames.get(i);
            int colLen = colInfo.get(i).size();
            for (int j = 0; j < colLen; j++){
                String columnName = colInfo.get(i).get(j).getName();
                List<ForeignKeyInfo> fkInfo = sm.getKeyInfo(tableName, columnName);
                fkInfoList.add(fkInfo);
            }
        }
        
        System.out.println("Введите имя файла xml для записи результата: ");
        String fileName = scan.next();
        
        XmlManager xm = new XmlManager();
        boolean res = xm.createXmlDocument();
        if (res){
            for(int i = 0; i < tableNamesLen; i++){
                xm.addTableInfo(tableNames.get(i), colInfo.get(i));
            }
        }
        else{
            System.out.println("Error");
            return;
        }
        
        int fkListLen = fkInfoList.size();
        for (int i = 0; i < fkListLen; i++){
            int fkLen = fkInfoList.get(i).size();
            for (int j = 0; j < fkLen; j++){
                xm.addRelationshipInfo(fkInfoList.get(i).get(j));
            }
        }
        xm.saveToFile(fileName);
        System.out.println("Запись успешно завершена");
    }  
}
