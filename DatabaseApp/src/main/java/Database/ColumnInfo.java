/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.List;

/**
 *
 * @author User
 * Contains information about one column of the table
 */
public class ColumnInfo {
    //column's name
    private String name;
    //table's name
    private String tableName;
    //information
    private List<Object> info;
    //type of information
    private String type;
    
    private Object minValue;
    private Object maxValue;
    private Object middleValue;
    private Object dispersion;
    
    private int amountOfExportedKeys;
    private List<DictionaryElement> dictElems;
    
    private List<BarChartElement> barChart;

    public List<BarChartElement> getBarChart() {
        return barChart;
    }

    public void setBarChart(List<BarChartElement> barChart) {
        this.barChart = barChart;
    }

    
    public int getAmountOfExportedKeys() {
        return amountOfExportedKeys;
    }

    public void setAmountOfExportedKeys(int amountOfExportedKeys) {
        this.amountOfExportedKeys = amountOfExportedKeys;
    }
            
    public Object getDispersion() {
        return dispersion;
    }

    public void setDispersion(Object dispersion) {
        this.dispersion = dispersion;
    }

    public Object getMiddleValue() {
        return middleValue;
    }

    public void setMiddleValue(Object middleValue) {
        this.middleValue = middleValue;
    }

    public List<DictionaryElement> getDictElems() {
        return dictElems;
    }

    public void setDictElems(List<DictionaryElement> dictElems) {
        this.dictElems = dictElems;
    }
    
    public ColumnInfo(){
        minValue = maxValue = null;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<Object> getInfo() {
        return info;
    }

    public void setInfo(List<Object> info) {
        this.info = info;
    }

    public Object getMinValue() {
        return minValue;
    }

    public void setMinValue(Object minValue) {
        this.minValue = minValue;
    }

    public Object getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Object maxValue) {
        this.maxValue = maxValue;
    }
}