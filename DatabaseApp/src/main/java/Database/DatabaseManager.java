/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
/*
 *
 * @author User
 */
public class DatabaseManager {
    Connection cn;
    
    //connects to the database
    public boolean connect(String user, String password, String dbName){
        Properties pr = new Properties();
        pr.setProperty("user", user);
        pr.setProperty("password", password);
        try{
            cn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/" + dbName, pr);
            return true;    
        }
        catch(Exception ex){
            return false;
        }    
    }
    
    public List<String> getTableNames(){
        //meta data about the table
        try{
            DatabaseMetaData md = cn.getMetaData();
        
            //get tables' names
            String[] types = {"TABLE"}; 
            ResultSet rs = md.getTables(null, null, "%", types);
            List<String> lsNames = new ArrayList<>();
            while(rs.next()){
                String name = rs.getString("TABLE_NAME");
                lsNames.add(name);
            }
            return lsNames;
        }
        catch(Exception ex){
            return null;
        }
    }
    
    public List<List<String>> getColumnNames(String tableName){
        try{
            //SQL query
            String sqlCommand = "SELECT * FROM " + tableName;
            PreparedStatement st = cn.prepareStatement(sqlCommand);
            ResultSet rsColumns = st.executeQuery();
            ResultSetMetaData rsmd = rsColumns.getMetaData();
            //get column amount
            int columnCount = rsmd.getColumnCount();
            
            //create containers to store info
            List<String> lsNames = new ArrayList<>();
            List<String> lsTypes = new ArrayList<>();   
            for (int i = 1; i <= columnCount; i++){
                //get column's name
                String name = rsmd.getColumnName(i);
                //get type of information stored in this column
                String typeName = rsmd.getColumnTypeName(i);
                lsNames.add(name);
                lsTypes.add(typeName);
            }
            List<List<String>> ls = new ArrayList<>();
            ls.add(lsNames);
            ls.add(lsTypes);
            return ls;
        }
        catch(Exception ex){
            return null;
        }
    }
    
    public List<Object> getInfoFromColumn(String columnName, String tableName){
        try{
            String command = "SELECT " + columnName + " FROM " + tableName;
            PreparedStatement stCol = cn.prepareStatement(command);
            ResultSet rsCol = stCol.executeQuery();
            List<Object> lsData = new ArrayList<>();
            while (rsCol.next()){
                Object obj = rsCol.getObject(columnName);
                
                lsData.add(obj);
            }
            return lsData;
        }
        catch(Exception ex){
            return null;
        }
    }
    
   /* public int getAmountOfExportedKeys(String tableName, String columnName){
        int amount = 0;
        try{
            DatabaseMetaData dmd = cn.getMetaData();
            ResultSet rs = dmd.getExportedKeys(null, null, tableName);
            while(rs.next()){
                String fkColumnName = rs.getString("PKCOLUMN_NAME");
                if (fkColumnName.equals(columnName))
                    amount++;
            }
        }
        catch(Exception ex){
            System.out.println(ex.toString());
        }
        return amount;
    }
    */
    public List<DictionaryElement> getData(String tableName, String columnName){
        try{
            String command = "SELECT " + columnName + " FROM " + tableName;
            PreparedStatement stCol = cn.prepareStatement(command);
            ResultSet rsCol = stCol.executeQuery();
            List<Object> lsData = new ArrayList<>();
            while (rsCol.next()){
                Integer number = rsCol.getInt(columnName);             
                lsData.add(number);
            }
            
            List<DictionaryElement> ls = getDictionary(lsData, "int4");
            return ls;
        }
        catch(Exception ex){
            return null;
        }
    }
    
    public List<DictionaryElement> getDictionary(List<Object> data, String type){
        if (!type.equalsIgnoreCase("varchar") && !type.equalsIgnoreCase("text") && !type.equalsIgnoreCase("int4") && !type.equalsIgnoreCase("float4") && !type.equalsIgnoreCase("date"))
            return null;
        List<DictionaryElement> ls = new ArrayList<>();
           
        int length = data.size();
        for (int i = 0; i < length; i++){
            int lengthLs = ls.size();
            boolean found = false;
            for (int j = 0; j < lengthLs && !found; j++){
                switch(type){
                    case "int4":
                        if ((int)ls.get(j).getValue() == (int)data.get(i)){
                            ls.get(j).setAmount(ls.get(j).getAmount() + 1);
                            found = true;
                        }
                        break;
                    case "float4":
                        if ((float)ls.get(j).getValue() == (float)data.get(i)){
                            ls.get(j).setAmount(ls.get(j).getAmount() + 1);
                            found = true;
                        }
                        break;
                    case "date": Date date = (Date)ls.get(j).getValue();
                        if (date.compareTo((Date)data.get(i)) == 0){
                            ls.get(j).setAmount(ls.get(j).getAmount() + 1);
                            found = true;
                        }
                        break;
                    default: String str = (String)ls.get(j).getValue();
                        if (str.compareTo((String)data.get(i)) == 0){
                            ls.get(j).setAmount(ls.get(j).getAmount() + 1);
                            found = true;
                        }
                        break;
                }
            }
            if (!found){
                DictionaryElement de = new DictionaryElement();
                de.setValue(data.get(i));
                de.setAmount(1);
                ls.add(de);
            }                   
        }   
        return ls;
    }
    
    public List<ForeignKeyInfo> getKeyInfo(String tableName, String columnName){
        List<ForeignKeyInfo> ls = new ArrayList<>();
        try{
            DatabaseMetaData dmd = cn.getMetaData();
            ResultSet rs = dmd.getExportedKeys(null, null, tableName);
            while(rs.next()){
                String pkColumnName = rs.getString("PKCOLUMN_NAME");
                if (pkColumnName.equals(columnName)){
                    String fkTableName = rs.getString("FKTABLE_NAME");
                    String fkColumnName = rs.getString("FKCOLUMN_NAME");
                    List<DictionaryElement> data = getData(fkTableName, fkColumnName);
                    int max = data.get(0).getAmount();
                    int min = data.get(0).getAmount();
                    double middleValue = data.get(0).getAmount();
                    double dispersion = 0;
                    int length = data.size();
                    for (int i = 1; i < length; i++){
                        if (data.get(i).getAmount() > max){
                            max = data.get(i).getAmount();
                        }
                        if (data.get(i).getAmount() < min){
                            min = data.get(i).getAmount();
                        }
                        
                        middleValue += data.get(i).getAmount();
                    }
                    
                    middleValue /= length;
                    
                    for (int i = 0; i < length; i++){
                        dispersion += Math.pow(((int)data.get(i).getAmount() - middleValue), 2);
                    }
                    dispersion /= (double)length;
                    dispersion = Math.sqrt(dispersion);
                    
                    ForeignKeyInfo fkInfo = new ForeignKeyInfo();
                    fkInfo.setPkTableName(tableName);
                    fkInfo.setPkColumnName(columnName);
                    fkInfo.setFkTableName(fkTableName);
                    fkInfo.setFkColumnName(fkColumnName);
                    fkInfo.setMax(max);
                    fkInfo.setMin(min);
                    fkInfo.setMiddleValue(middleValue);
                    fkInfo.setDispersion(dispersion);
                    
                    ls.add(fkInfo);
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex.toString());
        }
        finally{
            return ls;
        }
    }
    
    public boolean disconnect(){
        try{
            cn.close();
            return true;
        }
        catch(Exception ex){
            return false;
        }
    }
}
