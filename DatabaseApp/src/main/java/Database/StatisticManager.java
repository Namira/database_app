/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author User
 */
public class StatisticManager {
    private DatabaseManager dbm;
    
    public StatisticManager(){}
    
    public StatisticManager(String user, String password, String dbName){
        dbm = new DatabaseManager();
      //  dbm.connect("postgres", "2706537", "internetShop");
        dbm.connect(user, password, dbName);
    }
    
    public Object[] getMinMax(List<Object> clist, String type){
        int len = clist.size();
        if (len < 1)
            return null;
        if (!type.equalsIgnoreCase("int4") && !type.equalsIgnoreCase("date") && !type.equalsIgnoreCase("float4"))
            return null;
        Object maxVal = clist.get(0);
        Object minVal = clist.get(0);
        switch(type){   
            case "int4":    for (int i = 1; i < len; i++){
                                minVal = (int)clist.get(i) < (int)minVal ? (int)clist.get(i) : minVal;
                                maxVal = (int)clist.get(i) > (int)maxVal ? (int)clist.get(i) : maxVal;
                            } break;
            case "date":    Date min = (Date)minVal;
                            Date max = (Date)maxVal;
                            for (int i = 1; i < len; i++){
                                if (min.compareTo((Date)clist.get(i)) == 1){
                                    min = (Date)clist.get(i);
                                }
                                if (max.compareTo((Date)clist.get(i)) == -1){
                                    max = (Date)clist.get(i);
                                }
                            }
                            minVal = min;
                            maxVal = max;
                            break;
            case "float4":  for (int i = 1; i < len; i++){
                                minVal = (float)clist.get(i) < (float)minVal ? (float)clist.get(i) : minVal;
                                maxVal = (float)clist.get(i) > (float)maxVal ? (float)clist.get(i) : maxVal;
                            } break;
    }   
        
        Object[] result = new Object[2];
        result[0] = minVal;
        result[1] = maxVal;
        return result;
    }
    
    public Object getMiddleValue(List<Object> clist, String type){
        int len = clist.size();
        if (len < 1)
            return null;
        if (!type.equalsIgnoreCase("int4") && !type.equalsIgnoreCase("float4") && !type.equalsIgnoreCase("date"))
            return null;
        if (type.equalsIgnoreCase("date")){
            Calendar cl = Calendar.getInstance();
            cl.setTime((Date)(clist.get(0)));
            long middleDate = cl.getTimeInMillis();
            for (int i = 1; i < len; i++){
                cl.setTime((Date)(clist.get(i)));
                middleDate += cl.getTimeInMillis();
            }
            middleDate /= len;
            //return new Date(middleDate);
            return cl.get(Calendar.YEAR)+"-"+cl.get(Calendar.MONTH)+"-"+cl.get(Calendar.DAY_OF_MONTH);
        }
        if (type.equalsIgnoreCase("float4")){
            float middleVal = 0;
            for (int i = 0; i < len; i++){
                middleVal += (float)clist.get(i);
            }
            middleVal /= (float)len;
           
            return middleVal;
        }
        float middleInt = 0;
        for (int i = 0; i < len; i++){
            middleInt += (int)clist.get(i);
        }
        middleInt /= (float)len;
           
        return middleInt;
    }
    
    public Object getDispersion(List<Object> clist, String type, Object middleValue){
        int len = clist.size();
        if (len < 1)
            return null;
        if (!type.equalsIgnoreCase("int4") && !type.equalsIgnoreCase("float4"))
            return null;
        if (type.equalsIgnoreCase("float4")){
            double dispersion = 0;
            for (int i = 0; i < len; i++){
                dispersion += Math.pow(((float)clist.get(i) - (float)middleValue), 2);
            }
            dispersion = dispersion / (float)len;
            return dispersion;//dispersion;
        }
        double dispInt = 0;
        for (int i = 0; i < len; i++){
            dispInt += Math.pow(((int)clist.get(i) - (float)middleValue), 2);
            }
            dispInt /= (float)len;
            return dispInt;
    }
    
    public List<BarChartElement> getBarChart(List<Object> data, String type, int maxAmountOfIntervals){
        if (!type.equalsIgnoreCase("int4") && !type.equalsIgnoreCase("float4"))
            return null;
        if (maxAmountOfIntervals < 3)
            maxAmountOfIntervals = 3;
        List<BarChartElement> result = new ArrayList<>();
        
        int len = data.size();
        for (int i = 0; i < len; i++){
            int index = 0;
            int bcLen = result.size();
            double curElem;
            if (type.equalsIgnoreCase("int4")){
                Integer elem = (Integer)data.get(i);
                curElem = elem.doubleValue();
            }    
            else{
                Float elem = (Float)data.get(i);
                curElem = elem.doubleValue();
            }
           
            while (index <= bcLen){
                if (index < bcLen && curElem > result.get(index).getRightBorder()){
                    index++;
                    continue;
                }
                if (index < bcLen && curElem  >= result.get(index).getLeftBorder() && 
                    curElem <= result.get(index).getRightBorder()){
                    result.get(index).setAmount(result.get(index).getAmount() + 1);
                    break;
                }
                if (bcLen < maxAmountOfIntervals){
                    BarChartElement elem = new BarChartElement();
                    elem.setAmount(1);
                    elem.setLeftBorder(curElem);
                    elem.setRightBorder(curElem);
                    result.add(index, elem);
                    break;
                }
                if (index == 0){
                    //space between current element and the first element in the list
                    double space1 = result.get(0).getRightBorder() - curElem;
                    //space between the first and the second element in the list
                    double space2 = result.get(1).getRightBorder() - result.get(0).getLeftBorder();
                    if (space1 <= space2){
                        result.get(0).setAmount(result.get(0).getAmount() + 1);
                        result.get(0).setLeftBorder(curElem);
                    }
                    else{
                        result.get(1).setLeftBorder(result.get(0).getLeftBorder());
                        result.get(1).setAmount(result.get(0).getAmount() + result.get(1).getAmount());
                        result.get(0).setLeftBorder(curElem);
                        result.get(0).setRightBorder(curElem);
                        result.get(0).setAmount(1);
                    }
                    break;
                }
                if (index == bcLen){
                    index--;
                    double space1 = curElem - result.get(index).getLeftBorder();
                    double space2 = result.get(index).getRightBorder() - result.get(index - 1).getLeftBorder();
                    if (space1 <= space2){
                        result.get(index).setAmount(result.get(index).getAmount() + 1);
                        result.get(index).setRightBorder(curElem);
                    }
                    else{
                        result.get(index - 1).setRightBorder(result.get(index).getRightBorder());
                        result.get(index - 1).setAmount(result.get(index - 1).getAmount() + result.get(index).getAmount());
                        result.get(index).setLeftBorder(curElem);
                        result.get(index).setRightBorder(curElem);
                        result.get(index).setAmount(1);
                    }
                    break;
                }
                //space between current and previous
                double space1 = curElem - result.get(index - 1).getLeftBorder();
                double space2 = result.get(index).getRightBorder() - curElem;
                if (space1 < space2){
                    result.get(index - 1).setRightBorder(curElem);
                    result.get(index - 1).setAmount(result.get(index - 1).getAmount() + 1);
                    break;
                }
                if (space1 > space2){
                    result.get(index).setLeftBorder(curElem);
                    result.get(index).setAmount(result.get(index).getAmount() + 1);
                    break;
                }
                //space1 == space2
                double interval1 = result.get(index - 1).getRightBorder() - result.get(index - 1).getLeftBorder();
                double interval2 = result.get(index).getRightBorder() - result.get(index).getLeftBorder();
                if (interval1 < interval2){
                    result.get(index - 1).setRightBorder(curElem);
                    result.get(index - 1).setAmount(result.get(index - 1).getAmount() + 1);
                    break;
                }
                if (interval1 > interval2){
                    result.get(index).setLeftBorder(curElem);
                    result.get(index).setAmount(result.get(index).getAmount() + 1);
                    break;
                }
                if (result.get(index - 1).getAmount() < result.get(index).getAmount()){
                    result.get(index - 1).setRightBorder(curElem);
                    result.get(index - 1).setAmount(result.get(index - 1).getAmount() + 1);
                    break;
                }
                result.get(index).setLeftBorder(curElem);
                result.get(index).setAmount(result.get(index).getAmount() + 1);
                break;
            }
        }
        return result;
    }
    
    public List<String> getTableNames(){
        return dbm.getTableNames();
    }
    
     public List<ColumnInfo> getColumnInfo(String tableName, int maxAmountOfIntervals){
        //contains information about all columns of the table "tableName"
        List<ColumnInfo> clist = new ArrayList<>();
        //get all names and types of the table's columns: 0 - name, 1 - type
        List<List<String>> ls = dbm.getColumnNames(tableName);
        //number of columns
        int colNamesLen = ls.get(0).size();
        for (int i = 0; i < colNamesLen; i++){
            ColumnInfo ci = new ColumnInfo();
            ci.setTableName(tableName);
            ci.setName(ls.get(0).get(i));
            //gets all information from this column
            List<Object> colInfo = dbm.getInfoFromColumn(ls.get(0).get(i), tableName);
            String type = ls.get(1).get(i);
            ci.setType(type);
            ci.setInfo(colInfo);
            //gets min and max value: 0 - min, 1 - max
            Object[] range = getMinMax(colInfo, type);
            if (range != null){
                ci.setMinValue(range[0]);
                ci.setMaxValue(range[1]);
            }            
            
            Object middleVal = getMiddleValue(colInfo, type);
            ci.setMiddleValue(middleVal);
            
            Object dispersion = getDispersion(colInfo, type, middleVal);
            ci.setDispersion(dispersion);
            
            List<BarChartElement> listBc = getBarChart(colInfo, type, maxAmountOfIntervals);
            ci.setBarChart(listBc);
            
            clist.add(ci);
        }
        return clist;
    }
     
    public List<ForeignKeyInfo> getKeyInfo(String tableName, String columnName){
        return dbm.getKeyInfo(tableName, columnName);
    }
}
