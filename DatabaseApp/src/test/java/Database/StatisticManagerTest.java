/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexander Erin <arcquim@gmail.com>
 */
public class StatisticManagerTest {

    private List<Object> clist;
    private String type;
    private StatisticManager instance;

    @Before
    public void setUp() {
        Object[] numbers = { 1, 3, 7, 9, 11, 32, 32, 34 };
        clist = Arrays.asList(numbers);
        type = "int4";
        instance = new StatisticManager();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getMinMax method, of class StatisticManager.
     */
    @Test
    public void testGetMinMax() {
        System.out.println("getMinMax");
        Object[] expResult = { 1, 34 };
        Object[] result = instance.getMinMax(clist, type);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getMiddleValue method, of class StatisticManager.
     */
    @Test
    public void testGetMiddleValue() {
        System.out.println("getMiddleValue");
        Object expResult = (float)(129./8);
        Object result = instance.getMiddleValue(clist, type);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDispersion method, of class StatisticManager.
     */
    @Test
    public void testGetDispersion() {
        System.out.println("getDispersion");
        Object middleValue = (float)129./8;
        Object expResult = 3465./8 - (129./8)*(129./8);
        Object result = instance.getDispersion(clist, type, middleValue);
        assertEquals(expResult, result);
    }
    
}